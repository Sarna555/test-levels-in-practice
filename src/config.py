import os

kafka_brokers = os.getenv('KAFKA_BROKERS', 'localhost:29092')
kafka_topic_users_updates = 'user-updates'
